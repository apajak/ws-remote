# MultiPoint Services
- [README](README.md)
## Sommaire
- [Présentation de MultiPoint Services](#présentation-de-multipoint-services)
- [Scénarios d’utilisation courants](#scénarios-dutilisation-courants)
- [Stations MultiPoint](#stations-multipoint)
- [Sélection du matériel pour votre système MultiPoint Services](#sélection-du-matériel-pour-votre-système-multipoint-services)
- [Configuration matérielle requise et recommandations relatives aux performances](#configuration-matérielle-requise-et-recommandations-relatives-aux-performances)
- [Planification de site MultiPoint Services](#exemple-planification-de-site-multipoint-services)
- [Considérations relatives au réseau et comptes d’utilisateur](#considérations-relatives-au-réseau-et-comptes-dutilisateur)
- [Stockage de fichiers avec MultiPoint Services](#stockage-de-fichiers-avec-multipoint-services)
- [Protection du volume système avec la protection des disques](#protection-du-volume-système-avec-la-protection-des-disques)
- [Prise en charge de la virtualisation MultiPoint Services](#prise-en-charge-de-la-virtualisation-multipoint-services)
- [Considérations relatives aux applications](#considérations-relatives-aux-applications)
- [Liste de vérification de prédéploiement](#liste-de-vérification-de-prédéploiement)
## Présentation de MultiPoint Services

Le rôle MultiPoint Services dans Windows Server 2016 permet à plusieurs utilisateurs, chacun avec sa propre expérience Windows indépendante, de partager simultanément un même ordinateur. Il existe plusieurs façons pour que les utilisateurs puissent accéder à leurs sessions. L’une des façons est de se connecter à distance sur le serveur en utilisant les applications bureau à distance avec n’importe quel appareil. Un autre moyen est d’utiliser des stations physiques attachées au serveur MultiPoint :
- Directement aux ports vidéo sur l’ordinateur
- En utilisant des clients ultra-légers USB spécialisés (également appelés hubs USB multifonction), ainsi que des appareils USB-sur-Ethernet similaires.
- Sur le réseau local (LAN)

## Scénarios d’utilisation courants

MultiPoint Services fournit des bureaux d’utilisateur individuels avec les éléments les plus importants de l’expérience de bureau Windows 10. Il offre également un outil de gestion simple, le Gestionnaire MultiPoint, que les administrateurs système peuvent utiliser pour découvrir et contrôler plusieurs serveurs et clients MultiPoint. Par ailleurs, MultiPoint Services comprend le Tableau de bord MultiPoint qui donne une visibilité en temps réel. Voici quelques exemples de ce que vous pouvez faire avec MultiPoint Services :
- Donner à chaque utilisateur une expérience informatique personnelle et des dossiers privés sans ordinateur distinct pour chaque personne.
- Gérer plusieurs systèmes MultiPoint dans un labo informatique, une salle de classe, un centre de formation ou un environnement de petite entreprise.
- Installer un programme une fois et y accéder sur n’importe quelle station.
- Monitorer l’activité de bureau de chaque utilisateur.
- Bloquer les écrans avec un message personnalisable pour attirer l’attention du groupe.
- Limiter le groupe pour avoir accès uniquement à un ou plusieurs sites web.
- Projeter votre écran sur les autres écrans pour montrer une tâche particulière.
- Communiquer de manière privée avec un utilisateur standard qui demande de l’aide.
- Prendre le contrôle de la session d’un utilisateur pour montrer une tâche.
- Effectuer toutes les tâches listées ci-dessus pour un utilisateur qui utilise un PC traditionnel, un ordinateur portable ou tout autre appareil mobile.

## Stations MultiPoint

Dans un environnement système MultiPoint Services, les stations sont les points de terminaison utilisateur permettant de se connecter à l’ordinateur exécutant MultiPoint Services. Chaque station offre à l’utilisateur une expérience Windows 10 indépendante. Les types de station suivants sont pris en charge :
- Stations connectées à la vidéo directe
- Stations connectées au client USB zéro (y compris les clients USB sur Ethernet zéro)
- Stations connectées à RDP sur LAN (pour les ordinateurs clients riches ou clients légers)

Les PC complets sur utilisant le connecteur MultiPoint peuvent également être surveillés et contrôlés à l’aide du tableau de bord MultiPoint. Sur Windows 10 le connecteur MultiPoint peut être activé via le panneau de configuration pour les fonctionnalités Windows.

Multipoint Services prend en charge n’importe quelle combinaison de ces types de stations, mais il est recommandé qu’une station soit une station connectée directement à la vidéo, qui peut servir de station principale. La raison de cette recommandation est de pouvoir anticiper les scénarios de support. Par exemple, pour interagir avec le BIOS du système avant l’exécution de MultiPoint Services.

---
### Stations principales et stations standard
---
Une station à connexion vidéo directe est définie comme station principale. Les stations restantes sont appelées stations standard.
La station principale affiche les écrans de démarrage lorsque l’ordinateur est allumé. Il permet d’accéder aux informations de configuration du système et de résolution des problèmes qui ne sont disponibles que lors du démarrage. La station principale doit être une station vidéo connectée en direct. Après le démarrage, vous pouvez utiliser la station principale comme n’importe quelle autre station MultiPoint.

---
### Stations connectées à la vidéo directe
---
L’ordinateur exécutant MultiPoint Services peut contenir plusieurs cartes vidéo, chacune pouvant avoir un ou plusieurs ports vidéo. Cela vous permet de brancher des moniteurs pour plusieurs stations directement dans l’ordinateur. Les claviers et les souris sont connectés via des hubs USB associés à chaque moniteur. Ces hubs sont appelés hubs de station. D’autres périphériques, tels que des haut-parleurs, des casques ou des périphériques de stockage USB, peuvent également être connectés à un hub de station, et ils sont disponibles uniquement pour l’utilisateur de cette station.

```
IMPORTANT
Il doit y avoir au moins une station à connexion vidéo directe par serveur pour servir de station principale pour afficher le processus de démarrage lorsque l’ordinateur est allumé.
```
*SHEMA*
![multipoint_1](./pics/multipoint_1.gif)

---
### Stations connectées USB zéro client
---
Les stations connectées au client USB zéro utilisent un client USB zéro comme hub de station. Les clients USB zéro sont parfois appelés hubs multifonctions avec vidéo. Il s’agit d’un hub qui se connecte à l’ordinateur via un câble USB, et ces hubs prennent généralement en charge un moniteur vidéo, une souris et un clavier (PS/2 ou USB), l’audio et d’autres périphériques USB.
Le diagramme suivant montre un système serveur MultiPoint avec une station principale (station connectée par vidéo directe) et deux stations supplémentaires connectées au client USB zéro.

*SHEMA*
![multipoint_2](./pics/multipoint_2.gif)

---
### Clients zéro USB-over-Ethernet
---
Les clients zéro USB-over-Ethernet sont une variante des clients USB zéro qui envoient USB sur réseau local au système MultiPoint Services. Ces types de clients USB zéro fonctionnent de la même façon que d’autres clients USB zéro, mais ne sont pas limités par la longueur maximale des câbles USB. Les clients zéro USB-over-Ethernet ne sont pas des clients légers traditionnels, et ils apparaissent en tant que périphériques USB virtuels sur le système MultiPoint Services. La plupart des appareils disposent d’un plug-in tiers pour MultiPoint Manager qui vous permet d’associer et de connecter des appareils au système MultiPoint Services.

---
### Stations connectées RDP-over-LAN
---

Les clients légers et les ordinateurs de bureau, ordinateurs portables ou tablettes traditionnels peuvent se connecter à l’ordinateur exécutant MultiPoint Services via le réseau local (LAN) à l’aide du protocole RDP (Remote Desktop Protocol) ou d’un protocole propriétaire et du fournisseur de protocole Bureau à distance. Les connexions RDP offrent à l’utilisateur final une expérience très similaire à toute autre station MultiPoint, mais qui utilise le matériel de l’ordinateur client local.
Les clients et les appareils qui exécutent Microsoft RemoteFX peuvent offrir une expérience multimédia enrichie en tirant parti des fonctionnalités de processeur et de matériel vidéo du client léger ou de l’ordinateur local pour fournir des vidéos haute définition sur le réseau.
Si vous avez des clients LAN existants, MultiPoint Services peut fournir un moyen rapide et économique de mettre simultanément à niveau tous vos utilisateurs vers une expérience Windows 10.
Du point de vue du déploiement et de l’administration, les différences suivantes existent lorsque vous utilisez des stations connectées par RDP sur LAN :
- Non limité aux distances de connexion USB physiques
- Possibilité de réutiliser du matériel informatique plus ancien en tant que stations
- Plus facile à mettre à l’échelle vers un nombre plus élevé de stations. N’importe quel client de votre réseau peut potentiellement être utilisé comme station distante
- Aucun dépannage matériel via la console MultiPoint Manager
- Aucune fonctionnalité d’écran partagé.
- Aucun renommage de station ou configuration de l’ouverture de session automatique via la console du Gestionnaire MultiPoint

![multipoint_3](./pics/multipoint_3.gif)

---
### Options de configuration supplémentaires
---
### Stations à écran fractionné

MultiPoint Services offre une option d’écran partagé sur les ordinateurs dotés de stations connectées directement à la vidéo ou de stations connectées au client USB zéro. Un écran fractionné permet de créer une station supplémentaire par moniteur. Au lieu d’exiger deux moniteurs, vous pouvez utiliser un moniteur avec deux configurations de hub de station pour créer deux stations avec un seul moniteur. Vous pouvez rapidement augmenter le nombre de stations disponibles sans acheter de moniteurs supplémentaires, de clients USB-zéro ou de cartes vidéo.
Les avantages de l’utilisation d’une station à écran partagé peuvent être les suivants :
- Réduction des coûts et de la place nécessaire, puisque le système MultiPoint Services admet plus d’utilisateurs.

- Permettre à deux utilisateurs de collaborer côte à côte sur un projet.

- Possibilité pour l’enseignant d’expliquer une procédure sur une station tandis que le stagiaire suit la démonstration sur l’autre station.

- Tout écran de station MultiPoint Services ayant une résolution de 1024x768 ou plus peut être divisé en deux écrans de station. Pour une expérience utilisateur optimale, un écran large avec une résolution minimale de 1600 x 900 est recommandé. Un mini clavier sans pavé numérique est également recommandé pour permettre aux deux claviers de tenir devant le moniteur.
Pour créer des stations à écran partagé, vous configurez une station connectée directement à la vidéo ou connectée au client USB zéro. Ensuite, vous ajoutez un hub de station supplémentaire en branchant un clavier et une souris à un hub USB connecté au serveur. Vous pouvez ensuite convertir la station en deux stations à l’aide du Gestionnaire MultiPoint pour fractionner l’écran et mapper le nouveau hub à la moitié du moniteur. La moitié gauche de l’écran devient une station et la moitié droite devient une deuxième station.
Une fois une station divisée, un utilisateur peut se connecter à la station de gauche tandis qu’un autre utilisateur se connecte à la station de droite.

![multipoint_4](./pics/multipoint_4.gif)

---
### Comparaison des types de station
--- 

| Description                                                                                                                         | Vidéo directe connectée                                                                                                  | Client zéro USB connecté                                                                                                           | Connexion RDP-over-LAN                                                                                                             |
|-------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| Performances vidéo                                                                                                                  | Recommandé pour de meilleures performances vidéo                                                                         |                                                                                                                                    | Utiliser des clients légers qui prennent en charge RemoteFX pour améliorer la qualité vidéo à une bande passante réseau inférieure |
| Limitations physiques                                                                                                               | Limité par la longueur du câble vidéo et le hub USB et la longueur du câble (longueur maximale recommandée de 15 mètres) | Limité par la longueur du hub USB et du câble (longueur maximale recommandée de 15 mètres)                                         | Limité par la distribution LAN                                                                                                     |
| Nombre de stations autorisées                                                                                                       | Limité par le nombre d’emplacements PCIe disponibles sur la carte mère fois les ports vidéo par carte vidéo              | Le nombre total peut être limité par le fabricant du client USB zéro (pour plus d’informations, voir la note qui suit ce tableau.) | Limité par les ports disponibles sur le commutateur réseau                                                                         |
| Diviser l'écran                                                                                                                     | oui                                                                                                                      | oui                                                                                                                                | non                                                                                                                                |
| État du périphérique de la station MultiPoint Manager, configuration de l’ouverture de session automatique, renommage de la station | oui                                                                                                                      | oui                                                                                                                                | non                                                                                                                                |
| Accès aux menus de démarrage du serveur                                                                                             | oui                                                                                                                      | non                                                                                                                                | non                                                                                                                                |

## Sélection du matériel pour votre système MultiPoint Services
Lorsque vous créez un système MultiPoint Services, vous devez sélectionner un ordinateur qui répond à la Windows Server 2016 configuration système requise. Si vous décidez des composants à sélectionner, tenez compte des éléments suivants :
- Plage de prix cible de votre solution complète.
- Types de scénarios d’utilisation que vous pouvez attendre pour le système MultiPoint Services, par exemple, si les utilisateurs exécutent des programmes multimédias, utilisent des programmes de traitement de texte ou de productivité, ou naviguent sur Internet.
- Indique si votre scénario a des besoins de traitement ou de mémoire importants.
- Nombre d’utilisateurs qui peuvent utiliser le système en même temps. Si vous prévoyez d’avoir de nombreux utilisateurs sur votre système en même temps, ou des utilisateurs qui utilisent des programmes gourmands en système, vous devez prévoir plus de puissance de calcul pour votre système.
- Type de stations. De combien de ports USB ou de ports vidéo avez-vous besoin ?
- Plans d’expansion futurs. Envisagez-vous d’ajouter des stations au système MultiPoint Services à une date ultérieure ? Aurez-vous suffisamment d’emplacements de carte vidéo, de ports USB ou d’appuis réseau ? Combien d’utilisateurs supplémentaires votre matériel aura-t-il besoin de prendre en charge ?
- Disposition physique.

Un système MultiPoint Services comprend généralement les composants suivants :
- Un ordinateur qui exécute MultiPoint Services, qui comprend un processeur, une RAM, des disques durs et des cartes vidéo.
- Un moniteur, un hub de station, un clavier et une souris pour chaque station.
- Périphériques facultatifs pour les stations MultiPoint Services, y compris les haut-parleurs, les écouteurs, les microphones ou les périphériques de stockage disponibles uniquement pour l’utilisateur de la station.
- Périphériques facultatifs disponibles pour tous les utilisateurs du système MultiPoint Services, connectés directement à l’ordinateur hôte, tels que les imprimantes, les disques durs externes et les périphériques de stockage USB.

## Configuration matérielle requise et recommandations relatives aux performances
Les performances de votre système MultiPoint Services seront directement affectées par la capacité du processeur, du GPU et de la quantité de RAM disponible sur l’ordinateur exécutant MultiPoint Services.

Étant donné que MultiPoint Services est une solution de calcul des ressources partagées, le type et le nombre d’applications qui s’exécutent sur les stations peuvent avoir un impact sur les performances de votre système MultiPoint Services. Il est important de prendre en compte les types de programmes utilisés régulièrement lorsque vous planifiez votre système. Par exemple, une application gourmande en graphismes nécessite un ordinateur plus puissant qu’une application telle qu’un traitement de texte. La surcharge de l’ordinateur avec des applications gourmandes en graphismes entraînera probablement des problèmes de décalage dans l’ensemble du système.
Le type de contenu auquel les applications accèdent affecte également les performances du système. Si plusieurs stations utilisent des navigateurs web pour accéder à du contenu multimédia, tel que la vidéo en plein mouvement, moins de stations peuvent être connectées avant de nuire aux performances du système. À l’inverse, si plusieurs stations utilisent des navigateurs web pour accéder au contenu web statique, d’autres stations peuvent être connectées sans effet significatif sur les performances.

## Exemple Planification de site MultiPoint Services

*Laboratoire informatique Dans cette configuration, les stations sont disposées autour des murs de la salle, avec les étudiants face aux murs.*

![mutlipoint_5](./pics/multipoint_5.gif)

*Groupes Dans cette configuration, trois ordinateurs exécutent MultiPoint Services, avec des stations regroupées autour de chaque ordinateur.*

![mutlipoint_6](./pics/multipoint_6.gif)

*Salle de conférence Dans cette configuration, les stations sont configurées en lignes. L’avantage de cette configuration est que tous les étudiants sont confrontés à l’instructeur.*

![mutlipoint_7](./pics/multipoint_7.gif)

*Centre d’activités Cette configuration se compose d’une disposition de salle de conférence traditionnelle pour les bureaux, et elle a une zone distincte avec un seul ordinateur qui exécute MultiPoint Services avec ses stations associées.*

![mutlipoint_8](./pics/multipoint_8.gif)

*Bureau de petite entreprise Dans cette configuration, l’ordinateur qui exécute MultiPoint Services est placé dans un emplacement central et les utilisateurs du bureau s’y connectent à l’aide d’un réseau local (LAN).*

![mutlipoint_9](./pics/mulitpoint_9.gif)



## Considérations relatives au réseau et comptes d’utilisateur

MultiPoint services peut être déployé dans divers environnements réseau et peut prendre en charge des comptes d’utilisateur locaux et des comptes d’utilisateur de domaine. En règle générale, les comptes d’utilisateurs MultiPoint services sont gérés dans l’un des environnements réseau suivants :
- Un ordinateur unique exécutant MultiPoint services avec des comptes d’utilisateurs locaux
- Plusieurs ordinateurs exécutant MultiPoint services, chacun avec un compte d’utilisateur local
- Plusieurs ordinateurs exécutant MultiPoint services et qui utilisent des comptes d’utilisateur de domaine

Par définition, les comptes d’utilisateurs locaux ne sont accessibles qu’à partir de l’ordinateur sur lequel ils ont été créés. Les comptes d’utilisateurs locaux sont des comptes d’utilisateur qui sont créés sur un ordinateur spécifique qui exécute MultiPoint services. En revanche, les comptes d’utilisateur de domaine sont des comptes d’utilisateur qui résident sur un contrôleur de domaine et sont accessibles à partir de n’importe quel ordinateur connecté au domaine. Lorsque vous choisissez le type d’environnement réseau à utiliser, tenez compte des éléments suivants :

- Les ressources seront-elles partagées entre les serveurs ?
- Les utilisateurs vont-ils passer d’un serveur à un autre ?
- Les utilisateurs auront-ils accès aux serveurs de base de données qui nécessitent une authentification ?
- Les utilisateurs auront-ils accès aux serveurs Web internes qui nécessitent une authentification ?
- Existe-t-il une infrastructure de domaine Active Directory existante ?
- Qui utilisera la console MultiPoint Manager pour gérer les postes de travail des utilisateurs, afficher les miniatures, ajouter des utilisateurs, limiter les sites Web, etc. ? Cette personne gère-t-elle plus d’un serveur ? Cette personne doit disposer de privilèges d’administrateur sur les serveurs.

## Stockage de fichiers avec MultiPoint Services
MultiPoint Services prend en charge le stockage des fichiers utilisateur des manières suivantes :
- Sur la partition du système d’exploitation du lecteur de disque dur. Par défaut, MultiPoint Services stocke les fichiers utilisateur sur le disque dur avec le système d’exploitation.
- Sur une partition distincte du lecteur de disque dur. Lorsque le système MultiPoint services est configuré pour la première fois, vous pouvez partitionner le disque dur. Autrement dit, vous pouvez configurer une section du lecteur de sorte qu’elle fonctionne comme s’il s’agissait d’un lecteur distinct. Cela facilite la restauration ou la mise à niveau du système d’exploitation sans affecter les fichiers utilisateur.
- Sur un lecteur de disque dur interne ou externe supplémentaire. Vous pouvez attacher des lecteurs de disque dur internes ou externes supplémentaires à MultiPoint services pour enregistrer et sauvegarder des données.
- Dans un dossier réseau partagé. Pour rendre les fichiers utilisateur disponibles à partir de n’importe quelle station, vous pouvez créer un dossier partagé sur le réseau. Cela nécessite un autre ordinateur ou serveur en plus de l’ordinateur qui exécute MultiPoint services. Il s’agit de la méthode recommandée pour stocker des fichiers si un serveur de fichiers est disponible.

Pour les petits systèmes de 2-3 ordinateurs exécutant MultiPoint services sans serveur de fichiers disponible, l’un des ordinateurs MultiPoint services peut agir en tant que serveur de fichiers pour tous les ordinateurs MultiPoint services. Vous créez ensuite des comptes d’utilisateur pour tous les utilisateurs de MultiPoint services qui agit en tant que serveur de fichiers.

## Protection du volume système avec la protection des disques
MultiPoint services offre la possibilité d’effacer instantanément les modifications apportées au volume système à chaque démarrage de l’ordinateur. Si vous activez la fonctionnalité de protection des disques, toutes les modifications apportées au lecteur, telles que la configuration endommagée ou l’introduction de logiciels malveillants, sont annulées lors du redémarrage suivant de l’ordinateur. Il s’agit d’une fonctionnalité pratique pour les administrateurs qui souhaitent s’assurer qu’une image de logiciel « bonne » ou « Golden » connue est chargée à chaque fois. Les mises à jour automatiques ou les correctifs logiciels peuvent être planifiés, par exemple, au milieu de la nuit. La prise en compte de la planification consiste à indiquer si vous souhaitez que les utilisateurs finaux puissent apporter des modifications, telles que l’installation de logiciels, à partir d’Internet. Lorsque cette fonctionnalité est activée, si vous souhaitez que les utilisateurs puissent stocker des fichiers, le partage de fichiers doit être en dehors du volume système.

## Prise en charge de la virtualisation MultiPoint Services

MultiPoint Services prend en charge le rôle Hyper-V de deux manières :
- MultiPoint services peut être déployé en tant que système d’exploitation invité sur un serveur exécutant Hyper-V
- MultiPoint services peut être utilisé en tant que serveur de virtualisation.

L’exécution de MultiPoint services sur un ordinateur virtuel permet d’utiliser les outils Hyper-V pour gérer les systèmes d’exploitation. Ces outils incluent des fonctionnalités de point de contrôle et de restauration, et vous permettent d’exporter et d’importer des ordinateurs virtuels. Pour les installations plus volumineuses, vous pouvez consolider les serveurs en exécutant plusieurs ordinateurs virtuels MultiPoint services sur un serveur physique unique. Les scénarios possibles sont :

- Une seule salle de classe ou un seul atelier compte plus de 20 sièges. Plutôt que de déployer plusieurs ordinateurs physiques exécutant MultiPoint services, vous pouvez déployer plusieurs ordinateurs virtuels sur un même ordinateur physique.

    `Vous pouvez gérer plusieurs serveurs MultiPoint, qu’ils soient physiques ou virtuels, par le biais d’une console MultiPoint Manager unique.`

- Le serveur MultiPoint s’exécute sur un ordinateur virtuel avec une autre infrastructure de serveur sur le même ordinateur physique. Dans ce cas, cette infrastructure de serveur centralise le domaine, la sécurité et les données du réseau. Le serveur MultiPoint fournit Services Bureau à distance et centralise les postes de travail.

`Lors de l’exécution de MultiPoint services sur un ordinateur virtuel, les stations clientes USB-sur-Ethernet et RDP sont prises en charge. Les stations direct Video et USB connectées à zéro ne sont pas prises en charge.`


## Considérations relatives aux applications
### Compatibilité des applications
Toute application que vous souhaitez exécuter sur un système MultiPoint Services doit répondre aux exigences suivantes :
- Il doit s’installer et s’exécuter sur Windows Server 2016
- Il doit tenir compte de la session afin que chaque utilisateur puisse exécuter une instance de l’application dans un système MultiPoint.

## Liste de vérification de prédéploiement

1. Vérifiez que vos applications sont compatibles avec MultiPoint Services.

2. Déterminez le nombre d’utilisateurs susceptibles d’accéder, en même temps, à chaque ordinateur exécutant MultiPoint Services afin de pouvoir estimer le nombre d’ordinateurs requis qui doivent exécuter MultiPoint Services.

3. Comprendre les applications logicielles et le contenu web qui seront probablement accessibles par les utilisateurs, ainsi que l’impact que cela aura sur les performances du système.

4. Déterminez le nombre et le type de stations qui seront connectées au système.

5. Déterminez le matériel nécessaire.

6. Déterminez l’emplacement de votre système MultiPoint Services. Sera-t-il installé dans une seule pièce ou sera-t-il mis en place pour pouvoir être déplacé d’un emplacement à un autre?

7. Déterminez la façon dont les stations seront organisées.	

8. Vérifiez une infrastructure réseau et d’alimentation correcte.	

9. Déterminez comment les comptes d’utilisateur seront implémentés et gérés.

10. Déterminez comment les fichiers utilisateur seront partagés et stockés.	

[Retour README](README.md)