# Bureau à distance

- [README](README.md)

## Prérequis

Il est nécessaire de disposer d'une VM Windows Server 2016 préalablement créée,
d'allouer un groupe de sécurité "allow_ping_ssh_rdp",
et d'associer une adresse IP flottante.

Dans cette solution, vous trouverez les étapes pour :

- Télécharger et installer OpenSSH
- Autoriser l'accès dans le pare-feu Windows
- Se connecter à Windows via SSH depuis Linux ou Windows

## Télécharger et installer OpenSSH

1. Se connecter à la VM via RDP

2. Télécharger la dernière version d'OpenSSH server depuis GitHub (https://github.com/PowerShell/Win32-OpenSSH/releases). Dans notre cas, il s'agit de la version v8.1.0.0p1-Beta en 64 bits.

![bureau_distance1](./pics/bureau_distance/bureau_distance_1.png)

   Si vous ne pouvez pas télécharger le fichier, vous pouvez modifier les paramètres de sécurité.

   ![bureau_distance2](./pics/bureau_distance/bureau_distance_2.png)

   Aller dans "Panneau de configuration" → "Réseau et Internet" → "Options Internet" et choisir l'onglet "Sécurité"

   ![bureau_distance3](./pics/bureau_distance/bureau_distance_3.png)

   Cliquer sur le bouton "Niveau personnalisé..."

   ![bureau_distance4](./pics/bureau_distance/bureau_distance_4.png)

   Trouver la section "Téléchargements" et changer l'option "Téléchargement de fichiers" en "Activer", puis cliquer sur OK

   ![bureau_distance5](./pics/bureau_distance/bureau_distance_5.png)

   Confirmer en cliquant sur Oui, puis cliquer sur Appliquer

   ![bureau_distance6](./pics/bureau_distance/bureau_distance_6.png)

   Vous pouvez maintenant télécharger le fichier.

   ![bureau_distance7](./pics/bureau_distance/bureau_distance_7.png)

3. Ouvrir le fichier téléchargé et copier le dossier "OpenSSH-Win64" dans "C:\Program Files".

![bureau_distance8](./pics/bureau_distance/bureau_distance_8.png)

4. Aller dans "C:\Program Files\OpenSSH-Win64" et éditer le fichier "sshd_config_default".

![bureau_distance9](./pics/bureau_distance/bureau_distance_9.png)
![bureau_distance10](./pics/bureau_distance/bureau_distance_10.png)

5. Trouver et décommenter les lignes suivantes en supprimant le "#" au début, puis enregistrer le fichier.

   ```
    #Port 22
    #PasswordAuthentication yes
   ```

   Après les modifications, cela devrait être :

   ```
    Port 22
    PasswordAuthentication yes
   ```


6. Modifier la variable d'environnement système.

Pour cela, exécuter Windows PowerShell en tant qu'administrateur...

![bureau_distance11](./pics/bureau_distance/bureau_distance_11.png)

...et taper la commande suivante :

```
setx PATH "$env:path;C:\Program Files\OpenSSH-Win64" -m
```

Le message "SUCCESS: Specified value was saved." devrait s'afficher.

![bureau_distance12](./pics/bureau_distance/bureau_distance_12.png)

7. Se rendre dans le répertoire OpenSSH et exécuter le script d'installation.

```
cd "C:\Program Files\OpenSSH-Win64"; .\install-sshd.ps1
```

![bureau_distance13](./pics/bureau_distance/bureau_distance_13.png)


8. Autoriser le démarrage automatique et démarrer les services "sshd" et "ssh-agent".

```
Set-Service sshd -StartupType Automatic; Set-Service ssh-agent -StartupType Automatic; Start-Service sshd; Start-Service ssh-agent
```


## Autoriser l'accès dans le pare-feu Windows

Il est nécessaire d'ajouter une nouvelle règle de pare-feu pour ouvrir le port que nous utiliserons pour se connecter via SSH. Par défaut, il s'agit du port 22.

Pour ajouter la règle, exécuter la commande suivante dans Windows PowerShell :

```
New-NetFirewallRule -DisplayName "OpenSSH-Server-In-TCP" -Direction Inbound -LocalPort 22 -Protocol TCP -Action Allow
```

![bureau_distance14](./pics/bureau_distance/bureau_distance_14.png)

## Connecter à Windows via SSH

- Depuis Linux

Connectez-vous à la machine virtuelle en tapant la commande suivante dans le terminal. N'oubliez pas de saisir l'adresse IP de votre machine virtuelle.

``` ssh Administrator@<l'adresse-IP-de-votre-VM> ```
Saisissez le mot de passe.

Si vous êtes connecté, vous devriez être dans le répertoire personnel de l'administrateur.

![bureau_distance15](./pics/bureau_distance/bureau_distance_15.png)

- Depuis Windows (via Putty)

Ouvrez Putty et créez une nouvelle session. Remplissez les champs comme suit.

Nom d'hôte pour l'adresse IP : Administrator@<l'adresse-IP-de-votre-VM>
Port : 22
Type de connexion : SSH
Sessions enregistrées : <le-nom-de-votre-VM>

Cliquez sur le bouton Enregistrer.

![bureau_distance16](./pics/bureau_distance/bureau_distance_16.png)

Double-cliquez sur votre nouvelle session enregistrée pour vous connecter, puis cliquez sur Oui.

![bureau_distance17](./pics/bureau_distance/bureau_distance_17.png)


Vous êtes maintenant connecté à votre machine virtuelle Windows via SSH.

![bureau_distance18](./pics/bureau_distance/bureau_distance_18.png)